/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio03;

/**
 *
 * @author alunoti
 */
public class Exercicio03 {

    /**
     * Pergunta: Faça um algoritmo para calcular quantas ferraduras são necessárias para equipar
     * todos os 580 cavalos comprados para um haras
     * 
     * OBS: Levando em conta que cada cavalos (Normalmente)
     * 
     */
    public static void main(String[] args) {
       
        int cavalos,ferraduras;
        
        cavalos = 580;
        ferraduras = cavalos * 4;
        
        System.out.println("São necessários " + ferraduras + " ferraduras para equipar os " + cavalos + " cavalos.");
        
    }
    
}
